﻿# THE SECRET OF NASSAU

#LAST UPDATE
The Secret of Nassau was the winner (1st place) at this game jam competition, among 12 other games.

# DESCRIPTION
Game made in 48h to the Noord GameJam organized by NoordGames in July, 20-22, 2018. 
My main role in this project was as programmer, using Unreal 4 as engine. 
The theme of the jam was "Brazil-Netherlands", so we tried to escape from common subjects, 
potencially focused by many other groups, what led us to a story about Frans Post's paintings. 
The player is a friend of the famous "Maurício de Nassau" (Johan Maurits van Nassau-Siegen), 
the leader of the West India Company's expedition which tried to invade and colonize the recently 
discoverd lands of Brazil. Nassau asked his friend to save all the paintings (treasure in Europe) 
hidden in his house on Recife, Brazil. The player lives an immsersive experience where the bombs 
are falling, everything is shaking and he is trying to find all those arts spread by the house.

# TEAM
Programmers: Marcelino Borges, Rodrigo Silva
3D Artists: Caio Filizola, Eduardo Amorim
2D Artist: Vinícius Omena
SFX & Music: Vinícius Virtuoso
Game Designer: Danilo Lopes

# ArtStation

https://www.artstation.com/artwork/JA5Qa

#YOUTUBE
https://youtu.be/-PEkvmX_8rQ

